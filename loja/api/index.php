<?php
session_start();

require 'Slim/Slim.php';
require_once('DAL.php');
require_once('ShoppingCartController.php');
require_once('nusoap.php');


$app = new Slim();

$app->get('/catalog', 'getCatalog');
$app->get('/catalogBestSeller', 'getBestSellers');
$app->get('/orderCatalog', 'requestOrderCatalog');

$app->get('/cart/:id', '\ShoppingCart::get');
$app->post('/cart', '\ShoppingCart::addProduct');

$app->post('/sale/:id', function ($id) {
    $request = Slim::getInstance()->request();
    $cart = json_decode($request->getBody());
    echo DAL::getInstance()->addSale($id, $cart);

    $_SESSION['catalog'] = null;
    $ns = "urn:ImportMusicWS";

    $client = new nusoap_client('http://localhost/webservices/ImportMusicWS.php?wsdl');

    $result = $client->call('RegisterSale', array('storeName' => 'Loja', "Sales" => $request->getBody()), $ns);
});
$app->post('/import', 'import');
$app->put('/cart/:id', '\ShoppingCart::editProduct');
$app->delete('/cart/:id/:prod', '\ShoppingCart::removeProduct');

$app->post('/signin', 'signin');
$app->post('/signup', 'signup');
$app->get('/users', 'getUsers');
$app->post('/order', 'addOrder');
$app->run();


function addOrder()
{

    $request = Slim::getInstance()->request();
    //o que vem do import request é um array com o nome e a quantidade dos cds a importar
    $order = json_decode($request->getBody());


    foreach($order as $product) {
        $name = $product->Name;
        $quantity = $product->quantity;
        $image = $product->Img;
        $price = $product->Price;
        if(sendOrder( $name , $quantity)){
            DAL::getInstance()->addProductFromOrder($name, $image, $quantity, $price);
        }

    }

    DAL::getInstance()->addOrder($order);
    //TODO acrescentar codigo para ir buscar orders ao servico da editora

}

function getCatalog()
{

    if (!isset($_SESSION['catalog'])) {
        $_SESSION['catalog'] = json_encode(DAL::getInstance()->getProducts());
    }
    echo $_SESSION['catalog'];
}

function getBestSellers()
{
    echo(json_encode(DAL::getInstance()->getBestSellers()));
}

function requestOrderCatalog()
{
    $client = new SoapClient('http://172.31.101.22/wsArqsi/Service1.svc?Wsdl', array("trace" => 1, "exceptions" => 0));

    $result = $client->getCatalogo();

    $types = ['Rock', 'Eletronica', 'Pop', 'Kisomba', 'Rap', 'Disco'];
    $resultingArray = $result->GetCatalogoResult->Musica;
    foreach($resultingArray as $product) {
        $product->Type = $types[$product->Type];
        $product->Price = 10;
    }

    echo json_encode($resultingArray);
}

function sendOrder($name, $quantity)
{
    $client = new SoapClient('http://172.31.101.22/wsArqsi/Service1.svc?Wsdl', array("trace" => 1, "exceptions" => 0));
    $result = $client->SetEncomenda(array("api" => "a33621f7-83aa-44e8-b0d9-0b6169406c1c", "nome" => $name, "qtd" => $quantity));
    $response = $result->SetEncomendaResult ;

    return $response == "success" ? true : false;

}


function signin()
{
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());
    echo json_encode(DAL::getInstance()->getUser($user->username, $user->password));
}

function signup()
{
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());
    echo json_encode(DAL::getInstance()->insertUser($user->username, $user->password));
}

?>