<?php

class ShoppingCart
{

    function __construct()
    {
    }

    public static function  get($id)
    {
        echo json_encode(DAL::getInstance()->getShoppingCartFromUser($id));
    }

    public static function addProduct()
    {
        $request = Slim::getInstance()->request();
        $info = json_decode($request->getBody());
        DAL::getInstance()->insertProductIntoShoppingCart($info->userID, $info->productID, $info->quantity);
    }

    public static function editProduct($id)
    {
        $product = json_decode(Slim::getInstance()->request()->getBody());
        $productID = $product->id;
        $quantity = $product->quantity;
        DAL::getInstance()->updateProductInShoppingCart($id, $productID, $quantity);
    }

    public static function removeProduct($userID, $productID)
    {
        DAL::getInstance()->removeProductFromShoppingCart($userID, $productID);
    }


} 