<?php

class DAL
{
    public $connection;
    private static $instance;


    private function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=localhost;dbname=loja", "vitor", "vitor");
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    /**
     * This method checks if the user exists in the users table
     * @param $username user's username
     * @param $password user's password
     * @return array    array with success status, user id, username and role or just an array with fail status, empty username and role
     */
    public function getUser($username, $password)
    {


        try {
            $query = $this->connection->prepare('SELECT user_id, username, role from users where username = :username and password = :password');

            $query->bindParam(':username', $username);
            $query->bindParam(':password', $password);
            $query->execute();

            $result = $query->fetch();

        } catch (PDOException $e) {
            $this->log('fail', 'failed to authenticate user');
            return array('status' => 'fail', 'username' => '', 'role' => '');
        }

        if ($result) {
            $this->log('success', 'user authenticated with success');
            return array('status' => 'success', 'id' => $result["user_id"], 'username' => $result["username"], 'role' => $result["role"]);
        } else {
            $this->log('fail', 'username or password are wrong');
            return array('status' => 'fail', 'username' => '', 'role' => '');
        }
    }

    /**
     * This method inserts an user into the users table
     * @param $username user's username
     * @param $password user's password
     * @return array
     */
    public function insertUser($username, $password)
    {
        $data = array('username' => $username, 'password' => $password);

        try {
            $query = $this->connection->prepare("INSERT INTO users (username, password) value (:username, :password)");
            $query->execute($data);
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
            return array('status' => 'fail', 'error' => $e->getMessage());
        }

        $this->log('success', 'username or password are wrong');
        return array('status' => 'success');
    }

    public function getProducts()
    {

        try {
            $products = array();

            $query = $this->connection->prepare('SELECT * from products');
            $query->execute();

            $query->setFetchMode(PDO::FETCH_ASSOC);

            while ($result = $query->fetch()) {
                array_push($products, array('id' => $result['id'],
                    'name' => $result['name'],
                    'image' => $result['image'],
                    'stock' => $result['stock'],
                    'price' => $result['price']));
            }

            $this->log('success', 'got catalog with success');
            return $products;
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
            return array('status' => 'fail', 'error' => $e->getMessage());
        }
    }

    /**
     * @param $userID represents the id of the user adding a product to the shopping cart
     * @param $productID represents the id of the product being added to the shopping cart
     * @param $quantity the quantity of products being added to the shopping cart
     */
    public function insertProductIntoShoppingCart($userID, $productID, $quantity)
    {
        $data = array('userID' => $userID, 'productID' => $productID, 'quantity' => $quantity);

        try {
            $query = $this->connection->prepare("INSERT INTO cart (user_id, product_id, quantity) value (:userID, :productID, :quantity)");
            $query->execute($data);
            $this->log('success', "$quantity products were added to the shopping cart of the user with id $userID");

        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }

    }

    public function updateProductInShoppingCart($userID, $productID, $quantity)
    {
        $data = array('userID' => $userID, 'productID' => $productID, 'quantity' => $quantity);

        try {
            $query = $this->connection->prepare("UPDATE cart SET quantity = :quantity WHERE user_id = :userID and product_id = :productID");
            $query->execute($data);
            $this->log('success', 'updated product in cart with success');
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }
    }

    public function getShoppingCartFromUser($userID)
    {

        $data = array('userID' => $userID);

        try {
            $productIDS = array();
            $cart = array();

            $query = $this->connection->query("SELECT product_id, quantity FROM cart WHERE user_id = $userID");

            $query->setFetchMode(PDO::FETCH_ASSOC);

            while ($result = $query->fetch()) {
                array_push($productIDS, array('id' => $result['product_id'], 'quantity' => $result['quantity']));
            }

            if (count($productIDS) > 0) {

                foreach ($productIDS as $productID) {

                    $id = $productID['id'];
                    $quantity = $productID['quantity'];

                    $prodQuery = $this->connection->query("SELECT * FROM products WHERE id = $id");

                    $prodQuery->setFetchMode(PDO::FETCH_ASSOC);

                    while ($resultProd = $prodQuery->fetch()) {
                        array_push($cart, array('id' => $resultProd['id'],
                            'name' => $resultProd['name'],
                            'image' => $resultProd['image'],
                            'price' => $resultProd['price'],
                            'quantity' => $quantity));
                    }

                }
            }

            if (count($cart) == 0) {
                array_push($cart, 'No results');
                $this->log('success', 'user shopping cart is empty');
            }

            $this->log('success', 'got user shopping cart with success');
            return $cart;
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }
    }

    /**
     * @param $userID
     * @param $productID
     */
    public function removeProductFromShoppingCart($userID, $productID)
    {
        $data = array('userID' => $userID, 'productID' => $productID);

        try {
            $query = $this->connection->prepare("DELETE FROM cart WHERE user_id = :userID and product_id = :productID");
            $query->execute($data);

            $this->log('success', 'removed product from shopping cart');
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }
    }

    /**
     * First creates a sale in the database table and then it creates rows in the table sale_item associated with the  sale_od created
     * @param $order array with lines of the sale
     */
    public function addSale($userID, $sale)
    {
        $data = array('userID' => $userID);
        try {
            $query = $this->connection->prepare("INSERT INTO sales (user_id) value (:userID)");
            $query->execute($data);
            $sale_id = $this->connection->lastInsertId('sales');

            foreach ($sale as $product) {
                $data = array('saleID' => $sale_id, 'productID' => $product->id, 'quantity' => $product->quantity);
                $query = $this->connection->prepare("INSERT INTO sale_item (sale_id, product_id, qty) value (:saleID, :productID, :quantity)");
                $query->execute($data);

                $this->updateProductStock($product->id, $product->stock);
            }

            $this->log('success', 'a new sale has been added');
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }
    }

    public function updateProductStock($productID, $productQuantity)
    {

        $data = array('productID' => $productID, 'productQuantity' => $productQuantity);
        try {
            $query = $this->connection->prepare("UPDATE products SET stock = :productQuantity WHERE id = :productID");
            $query->execute($data);

        } catch (PDOException $e) {

        }
    }


    /**
     * First creates an order in the database table and then it creates rows in the table order_line associated with the  order_id created
     * @param $order array with lines of the order
     */
    public function addOrder($order)
    {
        try {

            $query = $this->connection->prepare("INSERT INTO orders () value ()");
            $query->execute();
            $order_id = $this->connection->lastInsertId('orders');

            $ids = [];

            foreach ($order as $product) {
                $query = $this->connection->prepare("SELECT id FROM products WHERE image = :image");
                $query->bindParam(':image', $product->Img);
                $query->execute();
                $result = $query->fetch();
                array_push($ids, array('id' => $result['id'], 'quantity' => $product->quantity));
            }


            foreach ($ids as $id) {

                $data = array('orderID' => $order_id, 'productID' => $id['id'], 'quantity' => $id['quantity']);
                $query = $this->connection->prepare("INSERT INTO order_line (order_id, product_id, qty) value (:orderID, :productID, :quantity)");
                $query->execute($data);
            }

            $this->log('success', 'a new order has been added');
        } catch (PDOException $e) {
            $this->log('fail', $e->getMessage());
        }
    }


    public function addProductFromOrder($name, $image, $quantity, $price)
    {

        try {

            $data = array("image" => $image);
            $query = $this->connection->prepare("SELECT COUNT(*) FROM products WHERE image = :image");
            $query->bindParam(':image', $image);
            $query->execute();
            if ($query->fetchColumn() > 0) {
                $data = array('quantity' => $quantity);
                $query = $this->connection->prepare("UPDATE products SET stock = stock + :quantity");
                $query->execute($data);
            } else {
                $data = array('name' => $name, 'image' => $image, 'quantity' => $quantity, 'price' => $price);
                $query = $this->connection->prepare("INSERT INTO products (name, image, stock, price) value (:name, :image, :quantity, :price)");
                $query->execute($data);
            }
        } catch (PDOException $e) {

        }
    }

    public function getBestSellers()
    {

        try {
            $query = $this->connection->query("SELECT SUM(qty) quantity,product_id FROM sale_item GROUP BY product_id ORDER BY quantity DESC ");

            $query->setFetchMode(PDO::FETCH_ASSOC);

            $productsBySells = array();
            while ($row = $query->fetch()) {
                $quantity = $row['quantity'];
                $product_id = $row['product_id'];

                $queryProduct = $this->connection->query("SELECT * FROM products WHERE id = $product_id");
                $result = $queryProduct->fetch();
                array_push($productsBySells,
                    array(
                        "id" => $result['id'],
                        "name" => $result['name'],
                        "image" => $result['image'],
                        "stock" => $result['stock'],
                        "price" => $result['price']
                    )
                );

            }
            $queryString = "SELECT * FROM products WHERE id not in (select id from products where ";

            $lastKey = array_search(end($productsBySells), $productsBySells);

            foreach ($productsBySells as $key => $product) {

                $queryString .= "id = " . $product['id'] . " ";
                if ($key != $lastKey) {
                    $queryString .= " or ";
                }

            }

            $queryString .= ")";

            $rest = $this->connection->query($queryString);


            while ($row = $rest->fetch()) {
                array_push($productsBySells,
                    array(
                        "id" => $row['id'],
                        "name" => $row['name'],
                        "image" => $row['image'],
                        "stock" => $row['stock'],
                        "price" => $row['price']
                    ));
            }

        } catch (PDOException $e) {
        }
        return $productsBySells;

    }

    /**
     * Logs actions to the database.
     * @param $status success or fail
     * @param $message information about whats being logged
     */
    public function log($status, $message)
    {

        $data = array('status' => $status, 'message' => $message);
        try {
            $query = $this->connection->prepare("INSERT INTO logs (status, message) value (:status, :message)");
            $query->execute($data);

        } catch (PDOException $e) {
        }
    }



    /* public function fillProducts() {

         $data = file_get_contents('../CDs.json');
         $aData = json_decode($data);

         foreach ($aData as $key => $product) {
             $info = array('name' => $product->name, 'image' => $product->image, 'price' => $product->price);
             $query = $this->connection->prepare("INSERT INTO products (name, image, price) value (:name, :image, :price)");
             $query->execute($info);
         }

     }*/

    /*
     * CREATE TABLE `sales`(`sale_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, `user_id` INT NOT NULL, `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (`user_id`) REFERENCES `users`(user_id) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE = InnoDB
     * */
} 