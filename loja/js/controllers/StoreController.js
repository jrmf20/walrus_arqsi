'use strict';
app.controller('StoreController', ['$scope', '$rootScope', 'ProductService', 'ShoppingCartService', 'AuthenticationService', function ($scope, $rootScope, ProductService, ShoppingCartService, AuthenticationServices) {

    $scope.title = 'Products';
    $scope.visible = false;
    $scope.choosen = 'name';
    $scope.loading = true;

    ProductService.getBestSellers().success(function (data) {
        $scope.products = data;
        $scope.loading = false;
    });

    $scope.addToCart = function (product, $event) {
        product.quantity++;
        ShoppingCartService.saveProduct(product);
    };

    $scope.defaultImage = 'http://i.imgur.com/f8xuRgs.png';

    $scope.isLogged = function () {
        return AuthenticationServices.isLogged();
    };

    $scope.hasntMore = function (product) {
        return product.stock <= 0 || product.quantity == product.stock;
    };

}]);

