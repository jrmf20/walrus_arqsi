'use strict';
app.controller('AuthenticationController', ['$scope', 'AuthenticationService', '$rootScope', function ($scope, AuthenticationService, $rootScope) {

    $scope.signin = function () {
        AuthenticationService.authenticateUser({'username': $scope.username, 'password': $scope.password}, $scope);
    };

    $scope.signout = function () {
        $rootScope.$broadcast('SIGNOUT');
        AuthenticationService.signoutUser();
    };

    $scope.signup = function () {
        AuthenticationService.signupUser({'username': $scope.username, 'password': $scope.password}, $scope);

    };

    $scope.isLogged = function () {
        return AuthenticationService.isLogged();
    };

    $scope.isAdmin = function() {
        return AuthenticationService.isAdmin();
    };

    $scope.getUsername = function() {
        return AuthenticationService.loggedUser();
    };

}]);
