'use strict';
app.controller('OrderController', ['$scope', '$rootScope', 'OrderService', 'ProductService', function ($scope, $rootScope, OrderService, ProductService) {

    $scope.loading = true;
    ProductService.getOrderCatalog().success(function (data) {
        //TODO improve this, check if addOrder and confirmOrder are working
        $scope.products = data;
        $scope.loading = false;

    });



    $scope.getOrdersQuantity = function () {
        return $scope.products.length;
    };

    $scope.addOrder = function (product) {
        OrderService.saveProduct(product);
    };

    $scope.confirmOrder = function () {
        OrderService.confirmOrder();
    };

}]);