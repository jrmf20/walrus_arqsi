'use strict';
app.controller('OrderListController', ['$scope', 'OrderService', function($scope, OrderService){


    $scope.removeOrder = function(index) {
        OrderService.deleteProduct(index);
    };

    $scope.orders = OrderService.getOrders();

    $scope.ordersQuantity = function(){
        return OrderService.getProductsQuantity();
    }

    $scope.totalPrice = function() {
        return OrderService.getTotalPrice();
    }

    $scope.confirmOrder = function() {
        OrderService.confirmOrder();
    }

}]);