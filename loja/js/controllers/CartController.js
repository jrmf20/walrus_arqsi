'use strict';
app.controller('CartController', ['$scope', 'ShoppingCartService', 'ProductService', '$rootScope', function ($scope, ShoppingCartService, ProductService, $rootScope) {

    $scope.selectedItem = 1;

    $scope.removeFromCart = function (index) {
        ShoppingCartService.deleteProduct(index);
    };

    $scope.cart = ShoppingCartService.getProductsInCart();

    $scope.getProductsQuantity = function () {
        return ShoppingCartService.getProductsQuantity();
    };

    $scope.totalPrice = function () {
        return ShoppingCartService.getTotalPrice();
    };

    $scope.checkout = function () {
        ShoppingCartService.checkout();
    };

    $scope.updateProduct = function (product) {
        ShoppingCartService.saveProduct(product);
    };

    $rootScope.$on('SIGNOUT', function () {
        ShoppingCartService.reset();
    });

}]);