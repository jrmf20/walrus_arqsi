'use strict';
app.service('CartAPI', ['$http', '$cookieStore', function ($http, $cookieStore) {

    var urlBase = 'api/cart';

    // GET /api/cart/1
    // Reads the shopping card from the user with id 1
    this.getCart = function () {
        return $http.get(urlBase + '/' + $cookieStore.get('user').id);
    };

    // POST api/cart
    // Inserts the product sent into the shopping cart of the current user
    this.insertProduct = function(product) {
        return $http.post(urlBase, {'userID': $cookieStore.get('user').id, 'productID': product.id, 'quantity': product.quantity} )
    };

    // PUT api/cart/1/
    // Edits the product sent from the shopping cart of the user with id 1
    this.updateProduct = function(product) {
        return $http.put(urlBase + '/' + $cookieStore.get('user').id, product);
    };

    // DELETE api/cart/1/10
    // Removes the product with id 10 from de shopping cart of the user with id 1
    this.removeProduct = function(product) {
        return $http.delete(urlBase + '/' + $cookieStore.get('user').id + '/' + product.id);
    };

    this.checkout = function(cart) {
        return $http.post('api/sale' + '/' + $cookieStore.get('user').id, cart);
    };


}]);