app.service('OrderService', ['$http', '$timeout', function ($http, $timeout) {

    var orders = [];
    var service = this;

    this.saveProduct = function (product) {

        if (service.hasProduct(product)) {
            product.quantity++;
        } else {
            product.quantity = 1;
            orders.push(product);
        }

    };

    this.getOrders = function () {
        return orders;
    };

    this.deleteProduct = function (index) {
        orders.splice(index, 1);
    };

    this.getProductsQuantity = function () {
        quantity = 0;
        orders.forEach(function (value) {
            quantity += value.quantity;
        });

        return quantity;
    };

    this.hasProduct = function (product) {
        found = false
        orders.forEach(function (value) {
            if (value.Name == product.Name) {
                found = true;
            }
        });
        return found;
    };

    this.getTotalPrice = function () {
        total = 0;

        orders.forEach(function (product) {

            total += product.quantity * product.Price;
        });

        return total;
    };

    this.confirmOrder = function () {
        return $http.post('api/order' , orders).success(function () {

            $timeout(function () {
                service.reset();
            }, 1000);
        });
    };

    this.reset = function () {
        while (orders.length > 0) {
            orders.pop();
        }
    };

}]);