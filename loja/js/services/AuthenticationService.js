app.service('AuthenticationService', ['$http', '$timeout', '$rootScope', '$location', '$cookieStore', function ($http, $timeout, $rootScope, $location, $cookieStore) {

    authentication = this;

    this.loggedUser = function () {
        if (this.isLogged()) {
            return $cookieStore.get('user') || {'username': 'Anonymous'};
        } else {
            return {'username': ' '};
        }
    };

    this.isAdmin = function () {

        if (this.isLogged()){
            return $cookieStore.get('user').role === "admin" ? true : false;
        }
        else
            return false;
    };
    this.authenticateUser = function (info, scope) {

        $http.post('api/signin', info).
            success(function (data, status, headers, config) {
                if (data.status === 'success') {
                    $cookieStore.put('user', data);
                    $location.path('/');
                } else {
                    scope.showError = true;
                    $timeout(function () {
                        scope.showError = false;
                    }, 2500);
                }
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    };

    this.signoutUser = function () {
        $cookieStore.remove('user');
        $location.path('/login');
    };

    this.signupUser = function (info, scope) {
        $http.post('api/signup', info).
            success(function (data, status, headers, config) {

                if (data.status === 'success') {
                    scope.showCreatedAccountMessage = true;
                    $timeout(function () {
                        scope.showCreatedAccountMessage = false;
                        authentication.authenticateUser(info, scope);
                    }, 2500);
                }
            }).
            error(function (data, status, headers, config) {
                return false;
            });
    };

    this.isLogged = function () {

        if ($cookieStore.get('user') == null)
            return false;
        else
            return true;

    };

}]);