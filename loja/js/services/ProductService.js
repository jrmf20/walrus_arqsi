app.service('ProductService', ['$http', function ($http) {

    this.Products = [];
    this.BestSellers = [];

    return {
        getProducts: function () {
            if (this.Products === undefined) {

                return $http.get('api/catalog').success(function (data) {
                    this.Products = data;
                    return this.Products;

                }).error(function (data) {
                    console.log('erro');

                });

            } else {
                return this.Products;
            }
        },

        getBestSellers: function() {
            if (this.BestSellers=== undefined) {

                return $http.get('api/catalogBestSeller').success(function (data) {
                    this.BestSellers = data;
                    return this.BestSellers;

                }).error(function (data) {
                    console.log('erro');

                });

            } else {
                return this.BestSellers;
            }
        },
        getOrderCatalog: function() {
            return $http.get('api/orderCatalog');
        },

        unitsOnStock: function(product) {

            stock = 0;
            this.Products.forEach(function (element) {
                if(element.id == product.id) {
                    stock = element.stock;
                }
            });

            return stock;
        }


    };
}]);