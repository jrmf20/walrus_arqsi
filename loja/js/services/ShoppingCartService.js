app.service('ShoppingCartService', ['CartAPI', '$timeout', function (CartAPI, $timeout) {

    var shoppingCart = [];
    var service = this;

    /**
     * Inserts the product into the shopping cart
     * @param product   the product to be added into the shopping cart
     */
    this.saveProduct = function (product) {
        if (service.hasProduct(product)) {
            CartAPI.updateProduct(product);
        } else {
            product.quantity = 1;
            shoppingCart.push(product);
            CartAPI.insertProduct(product);
        }

    };

    /**
     * Returns the list of products in the shopping cart
     * @returns {Array} the products in the shopping cart
     */
    this.getProductsInCart = function () {


       /* CartAPI.getCart().success(function (data) {
            if (data == 'No results') {
                shoppingCart = [];
            } else {
                shoppingCart = data;

            }
        }).error(function (data, status, headers, config) {
            console.log(status);
        });*/


        return shoppingCart;

    };

    /**
     * Deletes the product on the given index from the
     * shopping cart
     * @param index index of the product in the shopping
     * cart
     */
    this.deleteProduct = function (index) {
        shoppingCart[index].quantity = 0;
        CartAPI.removeProduct(shoppingCart[index]);
        shoppingCart.splice(index, 1);
    };

    /**
     * Sums the total of procuts in the cart, this uses
     * the property quantity of each product
     * @returns {number|*}
     */
    this.getProductsQuantity = function () {
        var quantity = 0;
        shoppingCart.forEach(function (value) {
            quantity += value.quantity;
        });
        return quantity;
    };

    /**
     * Checks if a product is in the shopping cart
     * @param product the product who are looking for
     * @returns {boolean|*} whether the product is in the cart or not
     */
    this.hasProduct = function (product) {
        var found = false;
        shoppingCart.forEach(function (value) {
            if (value.id == product.id) {
                found = true;
            }
        });
        return found;
    };

    this.getTotalPrice = function () {

        var total = 0;

        shoppingCart.forEach(function (product) {

            total += product.quantity * product.price;
        });

        return total;
    };

    this.checkout = function (checkingout) {

        shoppingCart.forEach(function(element){
            element.stock -= element.quantity;
        });

        CartAPI.checkout(shoppingCart).success(function () {

            $timeout(function () {
                service.reset();
            }, 1000);
        });



    };

    this.reset = function () {

        shoppingCart.forEach(function(element){
            element.quantity = 0;
        });

        while (shoppingCart.length > 0) {
            shoppingCart.pop();
        }
    };

}]);