var app = angular.module('StoreApp', ['ngRoute', 'ngCookies', 'ngAnimate']);

// configure our routes
app.config(function ($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'js/templates/home.html',
            controller: 'StoreController'
        })

        // route for the login page
        .when('/login', {
            templateUrl: 'js/view/LoginView.html',
            controller: 'AuthenticationController'
        })

        .when('/signup', {
            templateUrl: 'js/view/SignUpView.html',
            controller: 'AuthenticationController'
        })

        // route for the shopping cart
        .when('/shoppingCart', {
            templateUrl: 'js/view/CartView.html',
            controller: 'CartController'
        })

        // route for orders
        .when('/orders', {
            templateUrl: 'js/view/OrdersPageView.html',
            controller: 'OrderController'
        })

        // route for other paths
        .otherwise({
            templateUrl: 'js/templates/404.html'
        }
    );

});

app.run(function ($rootScope, $location, AuthenticationService) {


    var routesForAnonymous = ["js/view/LoginView.html", "js/view/SignUpView.html", "js/templates/404.html"];
    var routesForAdmin = ["js/view/OrdersPageView.html"];

    /**
     * True if the route doesnt require authentication
     * @param route a route
     * @returns {boolean|*} true if the route requires authentication or false if not
     */
    requiresAuthentication = function(route) {

        requires = true;

        routesForAnonymous.forEach(function (element) {
            if(element === route.templateUrl){
                requires =  false;
            }
        });

        return requires;
    };

    /**
     * True if the route doesnt require admin authentication
     * @param route
     * @returns {boolean|*} True if the route doesnt require admin authentication
     */
    requiresAdmin = function(route) {
        
        requires = false;
        
        routesForAdmin.forEach(function (element) {
            if(element === route.templateUrl){
                requires = true;
            }
        });

        return requires;
    };

    // Listens to the event $routeChangeStart and intercepts the route change to check some conditions
    $rootScope.$on("$routeChangeStart", function (event, next, current) {

                if (requiresAuthentication(next) && !AuthenticationService.isLogged()) {
                    $location.path("/login");

                } else if (requiresAdmin(next) && !AuthenticationService.isAdmin()){
                    console.log('tried to access admin page');
                    $location.path('/');
                }

    });
});